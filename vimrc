set nu
syntax on

call plug#begin()


Plug 'scrooloose/nerdtree', {'tag': '7.0.0', 'on':  'NERDTreeToggle' }

call plug#end()
